package com.dakarkaret.paginggit;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.userList)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        UserAdapter userUserAdapter = new UserAdapter();

        LiveData<PagedList<User>> userList = new LivePagedListBuilder<>(UserDataSource::new,
                new PagedList.Config.Builder()
                        .setInitialLoadSizeHint(10)
                        .setPageSize(20)
                        .build())
                .build();
        userList.observe(this, userUserAdapter::setList);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userUserAdapter);
    }
}