package com.dakarkaret.paginggit;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

class GitHubService {
    static WebApi getWebApi() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.github.com")
                .build()
                .create(WebApi.class);
    }

    public interface WebApi {
        @GET("/users")
        Single<List<User>> getUser(@Query("since") long since, @Query("per_page") int perPage);
    }
}