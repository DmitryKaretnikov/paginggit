package com.dakarkaret.paginggit;

import android.arch.paging.PagedListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

class UserAdapter extends PagedListAdapter<User, UserAdapter.UserItemViewHolder> {
    UserAdapter() {
        super(User.DIFF_CALLBACK);
    }

    @Override
    public UserItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list, parent, false));
    }

    @Override
    public void onBindViewHolder(UserItemViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    class UserItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.userLogin)
        TextView userLogin;
        @BindView(R.id.userName)
        TextView userId;

        UserItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(User user) {
            userLogin.setText(user.getLogin());
            userId.setText(String.valueOf(user.getId()));
        }
    }
}