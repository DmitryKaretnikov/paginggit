package com.dakarkaret.paginggit;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;

class User {
    static final DiffCallback<User> DIFF_CALLBACK = new DiffCallback<User>() {
        @Override
        public boolean areItemsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return oldItem.equals(newItem);
        }
    };

    private long id;
    private String login;

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != getClass())
            return false;

        User user = (User) obj;

        return id == user.id && login.equals(user.login);
    }

    long getId() {
        return id;
    }

    String getLogin() {
        return login;
    }
}