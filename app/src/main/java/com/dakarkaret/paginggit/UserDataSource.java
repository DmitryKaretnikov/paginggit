package com.dakarkaret.paginggit;

import android.arch.paging.ItemKeyedDataSource;
import android.support.annotation.NonNull;

class UserDataSource extends ItemKeyedDataSource<Long, User> {
    private final GitHubService.WebApi webApi;

    UserDataSource() {
        webApi = GitHubService.getWebApi();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<User> callback) {
        webApi.getUser(0, params.requestedLoadSize)
                .subscribe(callback::onResult);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<User> callback) {
        webApi.getUser(params.key, params.requestedLoadSize)
                .subscribe(callback::onResult);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<User> callback) {
    }

    @NonNull
    @Override
    public Long getKey(@NonNull User item) {
        return item.getId();
    }
}